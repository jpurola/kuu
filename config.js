'use stric';

module.exports = {

	path: {
		base: __dirname,
		global: require('global-modules'),
		service: './service',
		stub: './stub',
		launch: process.cwd()
	},

	stub: [
		{
			file: 'Kuu.yaml'
		},
		{
			file: 'Vagrantfile',
			replace: {
				NPM_GLOBAL: require('global-modules'),
			}
		}
	
	]


}