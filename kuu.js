#!/usr/bin/env node

const
_       = require('lodash'),
path   	= require('path'),
fs 		= require('fs'),
cfg    	= require('./config.js');


_.forEach(cfg.stub, function(stub) {

	fs.readFile(
		path.resolve(cfg.path.base, cfg.path.stub, stub.file),
		'utf8',
		function(err, data) {
			if (err) {
				throw 'Cannot read stub ' + stub.file
			} else {
				processFile(data, stub);
			}
		}
	)

});

function processFile(data, stub) {

	if (stub.hasOwnProperty('replace')) {

		_.forEach(stub.replace, function(replace, key) {

			key = '@@@' + key + '@@@';
			data = data.replace(key, replace);

		});

	}

	fs.writeFile(path.resolve(cfg.path.launch, stub.file), data, 'utf8', function(err) {

		if (err) {
			throw 'Couldn\'t write ' + stub.file;
		}

	});

}