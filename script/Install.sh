#!/usr/bin/env bash

if [ $(grep -c UTC /etc/timezone) -gt 0 ]
  then echo "#{timezone}" | sudo tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata
fi

sudo locale-gen en_US.UTF-8
cat /dev/null | sudo tee /etc/default/locale
echo 'LANG="en_US.UTF-8"' | sudo tee --append /etc/default/locale
echo 'LANGUAGE="en_US.UTF-8"' | sudo tee --append /etc/default/locale
echo 'LC_ALL="en_US.UTF-8"' | sudo tee --append /etc/default/locale
echo 'LC_CTYPE="en_US.UTF-8"' | sudo tee --append /etc/default/locale
sudo locale-gen en_US.UTF-8
sudo dpkg-reconfigure locales

# Add MongoDB Key
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

sudo apt-get update

sudo DEBIAN_FRONTEND=noninteractive apt-get upgrade -y
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y git mongodb-org build-essential tcl

# Install NVM
cd /home/vagrant
git clone https://github.com/creationix/nvm.git /home/vagrant/.nvm && cd /home/vagrant/.nvm && git checkout `git describe --abbrev=0 --tags`
export NVM_DIR="/home/vagrant/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
nvm install 6.3.0
chown -R vagrant:vagrant /home/vagrant/.nvm


cat << 'EOT' >> /home/vagrant/.profile

  # Load NVM
  export NVM_DIR="/home/vagrant/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"

  # Load NodeJS 6.3.0
  nvm use 6.3.0
EOT


# Install Redis
cd /home/vagrant
wget http://download.redis.io/releases/redis-stable.tar.gz
tar xzf redis-stable.tar.gz
cd /home/vagrant/redis-stable
make
#make test
sudo make install
sudo /home/vagrant/redis-stable/utils/install_server.sh